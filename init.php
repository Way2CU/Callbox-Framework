<?php

/**
 * Callbox Framework
 * Copyright © 2019 Way2CU. All Rights Reserved.
 *
 * This framework is a collection of functions which enable
 * developers to produce web hooks and scripts easily and without
 * repetitive implementations.
 *
 * Author: Mladen Mijatov
 */

require_once(__DIR__.'/framework/common/main.php');
require_once(__DIR__.'/framework/ctm/main.php');

?>
