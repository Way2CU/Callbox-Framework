<?php

/**
 * Call Tracking Metrics - Calls Handling
 *
 * Provides functions for interacting with CTM API. Before
 * using these functions `Callbox\configure` function must
 * be called.
 *
 * Author: Mladen Mijatov
 */

namespace CTM\Calls;

use CTM\Exception;
use CTM\Config;


/**
 * Get call details for specified call id. If call was not found
 * `null` is returned.
 *
 * @return object
 */
function get_details($call_id) {
	// prepare for call
	$url = Config::get_endpoint_url('/calls/{call_id}', array('call_id' => $call_id));
	$context = Config::get_context('GET');

	// get response from remote server
	$raw_response = file_get_contents($url, false, $context);
	$response = json_decode($raw_response);

	return $response;
}

/**
 * Find all calls for specified set of parameters. If no calls
 * were found empty array is returned.
 *
 * @param array $params
 * @return array
 */
function get_list($params=array(), $page=0, $per_page=100) {
	$result = array();

	// prepare for call
	$final_params = array(
			'page'     => $page,
			'per_page' => $per_page
		);
	$final_params = array_merge($final_params, $params);

	$url = Config::get_endpoint_url('/calls', null, $final_params);
	$context = Config::get_context('GET');

	// get response from remote server
	$raw_response = file_get_contents($url, false, $context);
	$response = json_decode($raw_response);

	// prepare response
	if ($response !== null && property_exists($response, 'calls'))
		$result = $response->calls;

	return $result;
}

/**
 * Destroy all personally identifiable information about a contact. Extra
 * care needs to be taken when setting `$redact_related` to `True`.
 *
 * Note: This action can not be undone!
 *
 * @param integer $call_id
 * @param boolean $redact_related
 * @return boolean
 */
function redact($call_id, $redact_related=false) {
	$result = false;
	$data = array('redact_related' => $redact_related);

	// prepare for call
	$url = Config::get_endpoint_url('/calls/{call_id}/redact', array('call_id' => $call_id));
	$context = Config::get_context('POST', array('Content-Type', 'application/json'), json_encode($data));

	// get response from remote server
	$raw_response = file_get_contents($url, false, $context);
	$response = json_decode($raw_response);

	// TODO: This needs to be fixed, CTM API doesn't specify response value.
	if ($response !== null)
		$result = true;

	return $result;
}

/**
 * Update specified call data.
 *
 * @param integer $call_id
 * @param array $data
 * @return boolean
 */
function update($call_id, $data=array()) {
	$result = false;

	// make sure we have data to work with
	if (!is_array($data) || count($data) == 0)
		throw new Exception('Unable to modify call with empty data!');

	// prepare for call
	$url = Config::get_endpoint_url('/calls/{call_id}/modify', array('call_id' => $call_id));
	$context = Config::get_context('POST', array('Content-Type', 'application/json'), json_encode($data));

	// get response from remote server
	$raw_response = file_get_contents($url, false, $context);
	$response = json_decode($raw_response);

	// TODO: Test stupid API and see what the response code is.
	if ($response !== null)
		$result = true;

	return $result;
}

/**
 * Add comment for current call.
 *
 * @param integer $call_id
 * @param string $text
 * @param string $user
 * @return integer
 */
function add_comment($call_id, $text, $user=null) {
	$result = null;
	$data = array('note' => $text);

	// include user if specified
	if (!is_null($user))
		$data['user'] = $user;

	// prepare for call
	$url = Config::get_endpoint_url('/calls/{call_id}/add_comment', array('call_id' => $call_id));
	$context = Config::get_context('POST', array('Content-Type', 'application/json'), json_encode($data));

	// get response from remote server
	$raw_response = file_get_contents($url, false, $context);
	$response = json_decode($raw_response);

	// TODO: Test stupid API and see what the response code is.
	if ($response !== null)
		$result = -1;

	return $result;
}

/**
 * Update existing comment.
 *
 * @param integer $call_id
 * @param integer $note_id
 * @param string $text
 * @param string $user
 * @return boolean
 */
function update_comment($call_id, $note_id, $text, $user=null) {
	$result = false;
	$data = array(
			'note_id' => $note_id,
			'note'    => $text
		);

	// include user if specified
	if (!is_null($user))
		$data['user'] = $user;

	// prepare for call
	$url = Config::get_endpoint_url('/calls/{call_id}/update_comment', array('call_id' => $call_id));
	$context = Config::get_context('POST', array('Content-Type', 'application/json'), json_encode($data));

	// get response from remote server
	$raw_response = file_get_contents($url, false, $context);
	$response = json_decode($raw_response);

	// TODO: Test stupid API and see what the response code is.
	if ($response !== null)
		$result = true;

	return $result;
}

/**
 * Remove specified comment from call.
 *
 * @param integer $call_id
 * @param integer $note_id
 */
function delete_comment($call_id, $note_id) {
	$result = false;
	$data = array('note_id' => $note_id);

	// prepare for call
	$url = Config::get_endpoint_url('/calls/{call_id}/delete_comment', array('call_id' => $call_id));
	$context = Config::get_context('POST', array('Content-Type', 'application/json'), json_encode($data));

	// get response from remote server
	$raw_response = file_get_contents($url, false, $context);
	$response = json_decode($raw_response);

	// TODO: Test stupid API and see what the response code is.
	if ($response !== null)
		$result = true;

	return $result;
}

/**
 * Get sale record and return object. If not sale was found `null` is returned.
 *
 * @param integer $call_id
 * @return object
 */
function get_sale($call_id) {
}

/**
 * Mark phone call as sale with optional data.
 *
 * @param integer $call_id
 * @param string $name        - Reporting tag within a call.
 * @param integer $score      - Number between 1 and 5 indicating quality of call.
 * @param boolean $conversion - Indicator whether call resulted in a sale.
 * @param numeric $value      - Numeric value indicating value of the call.
 * @param string $date        - Date of sale in YYYY-MM-DD format.
 * @return boolean
 */
function record_sale($call_id, $name=null, $score=null, $conversion=null, $value=null, $date=null) {
	$result = false;

	// prepare data
	$data = array(
			'account_id' => Config::get_account_id(),  // genius API design requires 2x account_id
			'call_id'    => $call_id
		);

	if (!is_null($name))
		$data['name'] = $name;

	if (!is_null($score))
		$data['score'] = $score;

	if (!is_null($conversion))
		$data['conversion'] = $conversion ? 1 : 0;

	if (!is_null($value))
		$data['value'] = $value;

	if (!is_null($date))
		$data['sale_date'] = $date;

	// prepare for call
	$url = Config::get_endpoint_url('/calls/{call_id}/sale', array('call_id' => $call_id));
	$context = Config::get_context('POST', array('Content-Type' => 'application/json'), json_encode($data));

	// get response from remote server
	$raw_response = file_get_contents($url, false, $context);
	$response = json_decode($raw_response);

	if ($response !== null)
		$result = !property_exists($response, 'error')
			&& property_exists($response, 'status')
			&& $response->status == 'success';

	return $result;
}

/**
 * Remove sale data from this call.
 *
 * @param integer $call_id
 */
function delete_sale($call_id) {
	$result = false;

	// prepare for call
	$url = Config::get_endpoint_url('/calls/{call_id}/sale', array('call_id' => $call_id));
	$context = Config::get_context('DELETE');

	// get response from remote server
	$raw_response = file_get_contents($url, false, $context);
	$response = json_decode($raw_response);

	// TODO: Test stupid API and see what the response code is.
	if ($response !== null)
		$result = true;

	return $result;
}

?>
