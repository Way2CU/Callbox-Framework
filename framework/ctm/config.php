<?php

/**
 * Call Tracking Metrics - Configuration
 *
 * Globally available configuration class which is used as
 * container by CTM functions.
 */

namespace CTM;

use Common;


final class Config {
	const API_URL = 'https://api.calltrackingmetrics.com/api/v1/accounts/{account_id}';

	private static $configured = false;
	private static $account_id;
	private static $key;
	private static $secret;

	/**
	 * Generate and return authentication header.
	 *
	 * @return string
	 */
	private static function get_authentication_header() {
		if (!self::$configured)
			throw new Exception('Access to CTM framework is not configured!');

		$auth = base64_encode(self::$key.':'.self::$secret);
		return 'Authorization: Basic '.$auth;
	}

	/**
	 * Apply configuration values for communicating with CTM.
	 *
	 * @param integer $account_id
	 * @param string $key
	 * @param string $secret
	 */
	public static function apply($account_id, $key, $secret) {
		self::$account_id = $account_id;
		self::$key = $key;
		self::$secret = $secret;
		self::$configured = true;
	}

	/**
	 * Return configured account id.
	 *
	 * @return integer
	 */
	public static function get_account_id() {
		return self::$account_id;
	}

	/**
	 * Generate stream context for sending requests. Only additional headers need
	 * to be specified. Headers for authentication with CTM are automatically included.
	 * Headers array is key/value pair instead of pre-formatted.
	 *
	 * Example:
	 *	Config::get_context(
	 *				'POST',
	 *				array(
	 *					'Content-type' => 'application/json'
	 *					),
	 *				json_encode($some_object)
	 *			);
	 *
	 * @param string $method
	 * @param array $headers
	 * @param string $content
	 * @return object
	 */
	public static function get_context($method='GET', $headers=array(), $content=null) {
		// prepare headers
		$final_headers = array();
		$final_headers []= self::get_authentication_header();

		if (!is_null($headers) && count($headers) > 0)
			foreach ($headers as $header => $value)
				$final_headers [] = $header.': '.$value;

		// create context and return
		return Common\get_context($method, $final_headers, $content);
	}

	/**
	 * Generate API endpoint URL and replace parameters with values
	 * provided in `$params`.
	 *
	 * Note: In addition to parameters specified in `$params` array it's
	 * also possible to specify `account_id` parameter, which will override
	 * default account ID provided by the configuration.
	 *
	 * Example:
	 *	Config::get_endpoint_url(
	 *				'/calls/{call_id}',
	 *				array('call_id' => $data->id)
	 *			);
	 *
	 * @param string $path
	 * @param array $params
	 * @param array $query
	 * @return string
	 */
	public static function get_endpoint_url($path, $params=array(), $query=array()) {
		// prepare replacement parameters
		$final_params = array('account_id' => self::$account_id);
		if (!is_null($params) && !empty($params))
			$final_params = array_merge($final_params, $params);

		// generate URL and return
		return Common\build_url(self::API_URL.$path, $final_params, $query);
	}
}

?>
