<?php

/**
 * Common functions for used by other parts of the framework.
 */

namespace Common;


/**
 * Generate stream context for sending requests. Only additional headers need
 * to be specified. Headers for authentication with CTM are automatically included.
 * Headers array is key/value pair instead of pre-formatted.
 *
 * Example:
 *	Config::get_context(
 *				'POST',
 *				array(
 *					'Content-type' => 'application/json'
 *					),
 *				json_encode($some_object)
 *			);
 *
 * @param string $method
 * @param array $headers
 * @param string $content
 * @return object
 */
function get_context($method='GET', $headers=array(), $content=null) {
	// prepare options
	$options = array('http' => array(
				'method'        => $method,
				'header'        => implode("\r\n", $headers),
				'ignore_errors' => true
			));

	// include content if specified
	if (!is_null($content))
		$options['http']['content'] = $content;

	return stream_context_create($options);
}

/**
 * Generate URL and replace parameters with values provided in `$params`.
 *
 * Example:
 *	build_url(
*			'https://api.calltrackingmetrics.com/calls/{call_id}',
*			array('call_id' => $data->id)
*		);
 *
 * @param string $path
 * @param array $params
 * @param array $query
 * @return string
 */
function build_url($url, $params=array(), $query=array()) {
	$result = $url;

	// replace parameters in path
	foreach ($final_params as $param => $value)
		$result = str_replace('{'.$param.'}', rawurlencode($value), $result);

	// include query params
	if (is_array($query) && !empty($params))
		$result .= '?'.build_query($query);

	return $result;
}

/**
 * Build query string the proper way. PHP's http_build_query doesn't know
 * how to properly create list items so we have to do it manually.
 *
 * @param array $params
 * @return string
 */
function build_query($params) {
	$result = '';

	foreach ($params as $key => $value)
		if (!is_array($value)) {
			// add normal value
			$result[] = $key.'='.rawurlencode($value);

		} else {
			// add param list
			foreach ($value as $list_item)
				$result[] = $key.'[]='.rawurlencode($list_item);
		}

	return implode('&', $result);
}

?>
