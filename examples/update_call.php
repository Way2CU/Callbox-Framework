<?php

/**
 * Update Call Tags
 *
 * This example updates call tags when being called by the CTM
 * webhook event. Incoming data is parsed by `CTM\parse_data` and
 * used to refer to call being updated.
 */

require_once('../init.php');
// require_once('/opt/framework/v1.0/init.php');

// configure access to remote service
CTM\configure(10000, 'key', 'secret');

// prepare data for tagging
$tags = array('new', 'sold');
$data = CTM\parse_data();

if ($data)
	CTM\Calls\update($data->id, array('tag_list' => implode(',', $tags)));

?>
